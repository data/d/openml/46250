# OpenML dataset: US-Births

https://www.openml.org/d/46250

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Number of births in the United States.

From original source:
-----
Number of births in the United States. There are several data sets covering different date ranges and
obtaining data from different sources.
-----

This dataset correspond to the births betweehn 1968-1988.

There are 4 columns:

id_series: The id of the time series.

date: The date of the time series in the format "%Y-%m-%d".

time_step: The time step on the time series.

value_0: The values of the time series, which will be used for the forecasting task.

Preprocessing:

1 - Created 'date' column with columns 'year', 'month', 'day', in the format %Y-%m-%d.

2 - Dropped columns 'year', 'month', 'day', 'day_of_year', 'day_of_week'.

3 - Created the column 'id_series' with value 0, there is only one long time series.

4 - Ensured that there are no missing dates and that the frequency of the time_series is daily.

5 - Created column 'time_step' with increasing values of time step for the time series.

6 - Renamed column 'births' to 'value_0'.

6 - Casted column 'value_0' to int. Defined 'id_series' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46250) of an [OpenML dataset](https://www.openml.org/d/46250). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46250/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46250/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46250/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

